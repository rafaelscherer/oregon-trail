function Traveler(name, food, isHealthy) {
    this.name = name;
    this.food = 1;
    this.isHealthy = true;    
}

Traveler.prototype.hunt = function() {
    this.food = this.food + 2;    
}

Traveler.prototype.eat = function() {
    
    if (this.food <= 0) {
        this.isHealthy = false;        
    } else {
        this.food = this.food - 1;
    }
}

function Wagon(capacity, passageiros) {
    this.capacity = capacity;
    this.passageiros = [];    
}

Wagon.prototype.getAvailableSeatCount = function() {
    return this.capacity - this.passageiros.length; 
}

Wagon.prototype.join = function(Traveler) {
    if (this.getAvailableSeatCount() > 0) {
        this.passageiros.push(Traveler);
    }
}

Wagon.prototype.shouldQuarantine = function() {
    return this.passageiros.some(passageiros => {
        if (passageiros.isHealthy === false) {
        return true;
        }
    })
}

Wagon.prototype.totalFood = function () {
    let total = 0;
    this.passageiros.forEach(Travelers => {
        total = total + Travelers.food;
    })
    return total;
}

// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);

